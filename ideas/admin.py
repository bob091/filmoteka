from django.contrib import admin
from django.utils.html import format_html
from .models import Idea, Vote

#klasa tworząca powiązanie pomiędzy głosami o pomysłami
class VoteInline(admin.StackedInline):
    model = Vote


@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
    search_fields =['title']
    list_display = ['title', 'decription','show_youtube_url','status']
    list_filter = ['status']
    inlines =[
        VoteInline,
    ]

    #metoda tworząca klikalny link , obj - odnisi się do obiektu
    def show_youtube_url(self, obj):
        if obj.youtube_url is not None:
            return format_html(f'<a href="{obj.youtube_url}" target="_blank">{obj.youtube_url}</a>')
        else:
            return ''

    #Zmiana nazwy kolumn
    show_youtube_url.short_description = 'YouTube URL'


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ['id','idea', 'reason']
    list_filter = ['idea']
