from django.shortcuts import render
from .models import Idea, Vote
from rest_framework import viewsets
from django.contrib.auth.models import User
from .serializer import IdeaSerializer, VoteSerializer, UserSerializer

class IdeaViewSet(viewsets.ModelViewSet):
    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer

class VoteViewSet(viewsets.ModelViewSet):
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

