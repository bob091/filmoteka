from .models import Idea, Vote
from django.contrib.auth.models import User
from rest_framework import serializers

class IdeaSerializer(serializers.HyperlinkedModelSerializer) :
    class Meta:
        model = Idea
        fields = ['id', 'title', 'decription', 'youtube_url', 'status']

class VoteSerializer(serializers.HyperlinkedModelSerializer) :
    class Meta:
        model = Vote
        fields = ['id', 'idea', 'reason']

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'is_staff']