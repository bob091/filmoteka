from django.db import models

IDEAS_STATUS = (
    ('pending', 'Czeka na akceptacje'),
    ('accepted', 'Zaakceptowany'),
    ('done', 'Zrealizowany'),
    ('rejected', 'Odrzucony'),
)

class Idea(models.Model):
    title = models.CharField(max_length=255)
    decription = models.TextField()
    youtube_url = models.URLField(null=True, blank=True)
    status = models.CharField(choices=IDEAS_STATUS, max_length=35, default='pending')

    #wyświetlanie nazwy obektu
    def __str__(self):
        return self.title

class Vote(models.Model):
    idea = models.ForeignKey(Idea, on_delete=models.CASCADE)
    reason = models.TextField() #Uzasadnienie oddania głosu

    # wyświetlanie nazwy obektu
    def __str__(self):
        return f'Vote ID : {self.id}'
